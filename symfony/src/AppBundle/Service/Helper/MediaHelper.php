<?php

namespace AppBundle\Service\Helper;

use AppBundle\Entity\Media\Folder;
use AppBundle\Entity\Media\Media;
use AppBundle\Exception\ApiException;
use AppBundle\Repository\FolderRepository;
use AppBundle\Repository\MediaRepository;
use AppBundle\Service\Builder\Tree\MediaTreeBuilder;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MediaHelper
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * @var FolderRepository
     */
    private $folderRepository;

    /**
     * @var MediaTreeBuilder
     */
    private $builder;

    public function __construct(EntityManagerInterface $entityManager, MediaTreeBuilder $builder)
    {
        $this->em = $entityManager;
        $this->mediaRepository = $entityManager->getRepository(Media::class);
        $this->folderRepository = $entityManager->getRepository(Folder::class);
        $this->builder = $builder;
    }

    /**
     * @param string|null $id
     * @return array
     * @throws ApiException
     */
    public function getFolderTree(?string $id): array
    {
        try {
            $folder = $id ? $this->folderRepository->find($id) : $this->folderRepository->finRootDir();
            $tree = $this->builder->build($folder);

            return $tree;
        } catch (ORMException $e) {
            throw new ApiException('Can\'t find folder', Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            throw new ApiException('Internal Error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteMediaItem(string $id = null)
    {
        if ($id) {
            $this->mediaRepository->removeMedia($id);
        }
    }

    public function deleteFolder(string $id = null)
    {
        if ($id) {
            $this->folderRepository->removeFolder($id);
        }
    }

    /**
     * @param string $name
     * @param string $parentId
     *
     * @throws ApiException
     */
    public function createFolder(string $name, string $parentId): void
    {
        $parent = $this->folderRepository->find($parentId);

        if (!$parent) {
            throw new ApiException('Can\'t find parent folder', Response::HTTP_BAD_REQUEST);
        }

        try {
            $folder = new Folder();
            $folder->setName($name);
            $folder->setParent($parent);
            $this->em->persist($folder);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new ApiException('Folder name already exists', Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            throw new ApiException('Internal error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
}
