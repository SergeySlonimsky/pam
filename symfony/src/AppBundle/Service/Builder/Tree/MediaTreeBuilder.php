<?php

namespace AppBundle\Service\Builder\Tree;

use AppBundle\Entity\Media\Folder;
use AppBundle\Entity\Media\Media;
use AppBundle\Repository\FolderRepository;
use AppBundle\Service\Builder\AbstractBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class MediaTreeBuilder extends AbstractBuilder
{
    /**
     * @var FolderRepository
     */
    protected $repository;

    /**
     * @var RequestStack
     */
    protected $request;

    public function __construct(EntityManagerInterface $manager, RequestStack $request)
    {
        parent::__construct($manager);
        $this->repository = $this->entityManager->getRepository(Folder::class);
        $this->request = $request;
    }

    /**
     * @param Folder $folder
     * @return array
     */
    public function build(Folder $folder): array
    {
        $tree = [
            'folder' => [],
            'children' => [],
            'media' => [],
        ];

        if (!$folder) {
            return $tree;
        }

        $tree['folder'] = [
            'id' => $folder->getId(),
            'name' => $folder->getName(),
            'parent' => $folder->getParent() ? [
                    'id' => $folder->getParent()->getId(),
                    'name' => $folder->getParent()->getName(),
                ] : null,
        ];

        if ($folder->getChildren()) {
            foreach ($folder->getChildren()->getValues() as $child) {
                /** @var $child Folder */
                $tree['children'][] = [
                    'id' => $child->getId(),
                    'name' => $child->getName(),
                ];
            }
        }

        if ($folder->getMedia()) {
            foreach ($folder->getMedia()->getValues() as $media) {
                /** @var $media Media */
                $tree['media'][] = [
                    'id' => $media->getId(),
                    'url' => $this->request->getCurrentRequest()->getUriForPath('/img/' . $media->getUrl()),
                ];
            }
        }

        return $tree;
    }
}
