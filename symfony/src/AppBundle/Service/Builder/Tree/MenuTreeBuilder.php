<?php

namespace AppBundle\Service\Builder\Tree;

use AppBundle\Entity\Page;
use AppBundle\Repository\PageRepository;
use AppBundle\Service\Builder\AbstractBuilder;
use Doctrine\ORM\EntityManagerInterface;

class MenuTreeBuilder extends AbstractBuilder
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct($manager);
        $this->repository = $this->entityManager->getRepository(Page::class);
    }

    public function build(string $menu): array
    {
        return $this->buildMenuTree($menu);
    }

    private function buildMenuTree(string $menu, $parent = null): array
    {
        $menuTree = [];
        $menuPages = $this->repository->getMenuPages($menu, $parent);
        /** @var Page $menuPage */
        foreach ($menuPages as $menuPage) {
            $menuTree[] = [
                'title' => $menuPage->getTitle(),
                'slug' => $menuPage->getSlug(),
                'childs' => $menuPage->getSubPages()->count()
                    ? $this->buildMenuTree($menu, $menuPage)
                    : [],
            ];
        }
        return $menuTree;
    }
}
