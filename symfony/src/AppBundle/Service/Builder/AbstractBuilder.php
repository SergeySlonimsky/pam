<?php

namespace AppBundle\Service\Builder;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractBuilder
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}
