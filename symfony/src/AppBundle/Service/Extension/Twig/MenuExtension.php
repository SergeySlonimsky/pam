<?php

namespace AppBundle\Service\Extension\Twig;

use AppBundle\Entity\Menu;
use AppBundle\Service\Builder\Tree\MenuTreeBuilder;

class MenuExtension extends \Twig_Extension
{
    /**
     * @var MenuTreeBuilder
     */
    private $menuBuilder;

    /**
     * @var \Twig_Environment
     */
    private $environment;

    public function __construct(MenuTreeBuilder $menuBuilder, \Twig_Environment $environment)
    {
        $this->menuBuilder = $menuBuilder;
        $this->environment = $environment;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('renderFooter', array($this, 'renderFooter')),
            new \Twig_SimpleFunction('renderHeader', array($this, 'renderHeader')),
            new \Twig_SimpleFunction('renderSidebar', array($this, 'renderSidebar')),
        );
    }

    public function renderHeader()
    {
        return $this->environment->render('site/partials/header.html.twig', [
            'items' => $this->menuBuilder->build(Menu::HEADER_MENU),
        ]);
    }

    public function renderFooter()
    {
        return $this->environment->render('site/partials/footer.html.twig', [
            'items' => $this->menuBuilder->build(Menu::FOOTER_MENU),
        ]);
    }

    public function renderSidebar()
    {
        return $this->environment->render('site/partials/sidebar.html.twig', [
            'items' => $this->menuBuilder->build(Menu::SIDE_MENU),
        ]);
    }
}
