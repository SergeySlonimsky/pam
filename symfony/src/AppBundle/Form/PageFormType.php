<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\Menu;
use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageFormType extends ResourceFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Page|null $page */
        $page = $options['page'];
        $builder
            ->add('menus', EntityType::class, [
                'class' => Menu::class,
                'by_reference' => false,
                'choice_label' => function (Menu $menu) {
                    return Menu::MENUS[$menu->getName()];
                },
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'choice_attr' => function() {
                    return ['class' => 'form-check-input'];
                }
            ])
            ->add('slug', TextType::class, [
                'empty_data' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('priority', IntegerType::class, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => false,
                'expanded' => false,
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);

        if (!$page->isSystem()) {
            $builder
                ->add('parent', EntityType::class, [
                    'class' => Page::class,
                    'query_builder' => function (EntityRepository $er) use ($page) {
                        $qb = $er->createQueryBuilder('p');
                        $qb->andWhere('p.isSystem = FALSE');

                        if ($page->getId()) {
                            $qb
                                ->andWhere('p.id != :page')
                                ->andWhere('p.parent != :parent OR p.parent IS NULL')
                                ->setParameter('page', $page->getId())
                                ->setParameter('parent', $page);
                        }

                        return $qb;
                    },
                    'choice_label' => function (Page $page) {
                        return $page->getTitle();
                    },
                    'group_by' => function(Page $parent) {
                        if ($parent->isVisible()) {
                            return 'Visible';
                        } else {
                            return 'Not visible';
                        }
                    },
                    'placeholder' => 'None',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                ->add('isVisible', CheckboxType::class, [
                    'required' => false,
                    'attr' => [
                        'class' => 'form-check-input',
                    ]
                ]);
        }

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
            'page' => null,
        ]);
    }
}
