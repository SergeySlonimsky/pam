<?php

namespace AppBundle\Form;

use AppBundle\Entity\Menu;
use AppBundle\Entity\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class MenuFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pages', Select2EntityType::class, [
                'required' => false,
                'multiple' => true,
                'class' => Page::class,
                'minimum_input_length' => 2,
                'scroll' => false,
                'text_property' => 'title',
                'property' => 'name',
                'remote_route' => 'admin_page_get_all',
                'label' => false,
            ]);

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);

        parent::configureOptions($resolver);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['name'] = Menu::MENUS[$form->getData()->getName()];
    }
}
