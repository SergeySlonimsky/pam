<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MenuRepository")
 * @ORM\Table(name="menu")
 */
class Menu
{
    const HEADER_MENU = 'header';
    const FOOTER_MENU = 'footer';
    const SIDE_MENU = 'side';

    /** @var array  */
    const MENUS = [
        self::HEADER_MENU => 'Хедер',
        self::FOOTER_MENU => 'Футер',
        self::SIDE_MENU => 'Сайдбар',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(visible=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Page", inversedBy="menus")
     * @ORM\JoinTable(name="pages_menu")
     */
    private $pages;

    public function __construct()
    {
        $this->pages =  new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Menu
     */
    public function setId(int $id): Menu
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Menu
     */
    public function setName(string $name): Menu
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    /**
     * @param Collection $pages
     * @return Menu
     */
    public function setPages(Collection $pages): Menu
    {
        $this->pages = $pages;
        return $this;
    }

    /**
     * @param Page $page
     * @return Menu
     */
    public function addPage(Page $page): Menu
    {
        $this->pages->add($page);

        return $this;
    }

    /**
     * @param Page $page
     * @return Menu
     */
    public function removePage(Page $page): Menu
    {
        $this->pages->removeElement($page);

        return $this;
    }
}
