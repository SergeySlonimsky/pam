<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Media\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(title="Id", operatorsVisible=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @GRID\Column(title="Description", operatorsVisible=false)
     */
    private $description;

    /**
     * @var float|null
     *
     * @ORM\Column(type="float")
     *
     * @GRID\Column(title="Price", operatorsVisible=false)
     */
    private $price;

    /**
     * @var Collection|null
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", inversedBy="products")
     * @ORM\JoinTable(name="product_to_category")
     */
    private $categories;

    /**
     * @var Collection|null
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Media\Media", inversedBy="products")
     * @ORM\JoinTable(name="media_to_product")
     */
    private $media;

    public function __construct()
    {
        $this->media = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Product
     */
    public function setName(?string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return Product
     */
    public function setDescription(?string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return Product
     */
    public function setPrice(?float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getCategories(): ?Collection
    {
        return $this->categories;
    }

    /**
     * @param Collection|null $categories
     * @return Product
     */
    public function setCategories(?Collection $categories): Product
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param Category $category
     * @return Product
     */
    public function addCategory(Category $category): Product
    {
        $this->categories->add($category);
        return $this;
    }

    /**
     * @param Category $category
     * @return Product
     */
    public function removeCategory(Category $category): Product
    {
        $this->categories->removeElement($category);
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getMedia(): ?Collection
    {
        return $this->media;
    }

    /**
     * @param Collection|null $media
     * @return Product
     */
    public function setMedia(?Collection $media): Product
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param Media $media
     * @return Product
     */
    public function addMedia(Media $media): Product
    {
        $this->media->add($media);

        return $this;
    }

    /**
     * @param Media $media
     * @return Product
     */
    public function removeMedia(Media $media): Product
    {
        $this->media->removeElement($media);

        return $this;
    }
}
