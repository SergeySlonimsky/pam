<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 * @ORM\Table(name="page")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(visible=false)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Title", operatorsVisible=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @GRID\Column(visible=false)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @GRID\Column(title="Priority", operatorsVisible=false)
     */
    private $priority;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Slug", operatorsVisible=false)
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @GRID\Column(title="Is Visible", operatorsVisible=false, size="100")
     */
    private $isVisible;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @GRID\Column(title="System", operatorsVisible=false)
     */
    private $isSystem;

    /**
     * @var Page|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Page", inversedBy="subPages")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     * @GRID\Column(field="parent.title", title="Parent Page", operatorsVisible=false)
     */
    private $parent;

    /**
     * @var Collection|null
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Menu", mappedBy="pages")
     *
     * @GRID\Column(visible=false)
     */
    private $menus;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Page", mappedBy="parent")
     */
    private $subPages;

    /**
     * @var Category|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="pages")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    public function __construct()
    {
        $this->isSystem = false;
        $this->isVisible = false;
        $this->subPages = new ArrayCollection();
        $this->menus = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Page
     */
    public function setId(int $id): Page
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Page
     */
    public function setTitle(string $title): Page
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param null|string $content
     * @return Page
     */
    public function setContent(?string $content): Page
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return Page
     */
    public function setPriority(int $priority): Page
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     * @return Page
     */
    public function setSlug(?string $slug): Page
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     * @return Page
     */
    public function setIsVisible(bool $isVisible): Page
    {
        $this->isVisible = $isVisible;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSystem(): bool
    {
        return $this->isSystem;
    }

    /**
     * @param bool $isSystem
     * @return Page
     */
    public function setIsSystem(bool $isSystem): Page
    {
        $this->isSystem = $isSystem;
        return $this;
    }

    /**
     * @return Page|null
     */
    public function getParent(): ?Page
    {
        return $this->parent;
    }

    /**
     * @param Page|null $parent
     * @return Page
     */
    public function setParent(?Page $parent): Page
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return PersistentCollection|null
     */
    public function getSubPages(): ?PersistentCollection
    {
        return $this->subPages;
    }

    /**
     * @param Page $page
     * @return Page
     */
    public function removeSubPage(Page $page): Page
    {
        $this->subPages->removeElement($page);

        return $this;
    }

    /**
     * @param Page $page
     * @return Page
     */
    public function addSubPage(Page $page): Page
    {
        $this->subPages->add($page);

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getMenus(): ?Collection
    {
        return $this->menus;
    }

    /**
     * @param Collection|null $menus
     * @return Page
     */
    public function setMenus(?Collection $menus): Page
    {
        $this->menus = $menus;

        return $this;
    }

    /**
     * @param Menu $menu
     * @return Page
     */
    public function addMenu(Menu $menu): Page
    {
        $this->menus->add($menu);
        $menu->addPage($this);

        return $this;
    }

    /**
     * @param Menu $menu
     * @return Page
     */
    public function removeMenu(Menu $menu): Page
    {
        $this->menus->removeElement($menu);
        $menu->removePage($this);

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     * @return Page
     */
    public function setCategory(?Category $category): Page
    {
        $this->category = $category;
        return $this;
    }
}
