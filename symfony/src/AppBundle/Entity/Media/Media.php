<?php

namespace AppBundle\Entity\Media;

use AppBundle\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaRepository")
 * @ORM\Table(name="media_media")
 */
class Media
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var Folder|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Media\Folder", inversedBy="media")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    private $folder;

    /**
     * @var Collection|null
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Product", mappedBy="media")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Media
     */
    public function setId(int $id): Media
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Media
     */
    public function setUrl(string $url): Media
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return Folder|null
     */
    public function getFolder(): ?Folder
    {
        return $this->folder;
    }

    /**
     * @param Folder|null $folder
     * @return Media
     */
    public function setFolder(?Folder $folder): Media
    {
        $this->folder = $folder;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    /**
     * @param Collection|null $products
     * @return Media
     */
    public function setProducts(?Collection $products): Media
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @param Product $product
     * @return Media
     */
    public function addProduct(Product $product): Media
    {
        $this->products->add($product);
        return $this;
    }

    /**
     * @param Product $product
     * @return Media
     */
    public function removeProduct(Product $product): Media
    {
        $this->products->removeElement($product);
        return $this;
    }
}
