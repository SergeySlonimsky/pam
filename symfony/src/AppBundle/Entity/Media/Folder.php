<?php

namespace AppBundle\Entity\Media;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FolderRepository")
 * @ORM\Table(name="media_folder", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_path", columns={"name", "parent_id"})})
 */
class Folder
{
    const ROOT_FOLDER = 'root';

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Media\Folder", mappedBy="parent")
     */
    private $children;

    /**
     * @var Folder|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Media\Folder", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Media\Media", mappedBy="folder")
     */
    private $media;

    public function __construct()
    {
        $this->media = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Folder
     */
    public function setId(int $id): Folder
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Folder
     */
    public function setName(string $name): Folder
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getMedia(): ?Collection
    {
        return $this->media;
    }

    /**
     * @param Collection|null $media
     * @return Folder
     */
    public function setMedia(?Collection $media): Folder
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param Media $media
     * @return Folder
     */
    public function addMedia(Media $media): Folder
    {
        $this->media->add($media);

        return $this;
    }

    /**
     * @param Media $media
     * @return Folder
     */
    public function removeMedia(Media $media): Folder
    {
        $this->media->removeElement($media);

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    /**
     * @param Collection|null $children
     * @return Folder
     */
    public function setChildren(?Collection $children): Folder
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @param Folder $child
     * @return Folder
     */
    public function addChildren(Folder $child): Folder
    {
        $this->children->add($child);
        return $this;
    }

    /**
     * @param Folder $child
     * @return Folder
     */
    public function removeChildren(Folder $child): Folder
    {
        $this->children->removeElement($child);
        return $this;
    }

    /**
     * @return Folder|null
     */
    public function getParent(): ?Folder
    {
        return $this->parent;
    }

    /**
     * @param Folder|null $parent
     * @return Folder
     */
    public function setParent(?Folder $parent): Folder
    {
        $this->parent = $parent;
        return $this;
    }
}
