<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(title="Id", operatorsVisible=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @GRID\Column(title="Description", operatorsVisible=false)
     */
    private $description;

    /**
     * @var Collection|null
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Product", mappedBy="categories")
     */
    private $products;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Page", mappedBy="category")
     */
    private $pages;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->pages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Category
     */
    public function setName(?string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return Category
     */
    public function setDescription(?string $description): Category
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getPages(): ?Collection
    {
        return $this->pages;
    }

    /**
     * @param Collection|null $pages
     * @return Category
     */
    public function setPages(?Collection $pages): Category
    {
        $this->pages = $pages;
        return $this;
    }

    /**
     * @param Page $page
     * @return Category
     */
    public function addPage(Page $page): Category
    {
        $this->pages->add($page);
        return $this;
    }

    /**
     * @param Page $page
     * @return Category
     */
    public function removePage(Page $page): Category
    {
        $this->pages->removeElement($page);
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    /**
     * @param Collection|null $products
     * @return Category
     */
    public function setProducts(?Collection $products): Category
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @param Product $product
     * @return Category
     */
    public function addProduct(Product $product): Category
    {
        $this->products->add($product);
        return $this;
    }

    /**
     * @param Product $product
     * @return Category
     */
    public function removeProduct(Product $product): Category
    {
        $this->products->removeElement($product);
        return $this;
    }
}
