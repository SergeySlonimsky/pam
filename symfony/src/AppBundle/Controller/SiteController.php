<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Media\Media;
use AppBundle\Entity\Page;
use AppBundle\Entity\Product;
use AppBundle\Repository\PageRepository;
use AppBundle\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SiteController extends Controller
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->pageRepository = $entityManager->getRepository(Page::class);
        $this->productRepository = $entityManager->getRepository(Product::class);
    }

    public function indexAction()
    {
        $items = array_slice($this->productRepository->findAll(), 0, 6);

        return $this->render('site/index.html.twig', [
            'items' => $items,
        ]);
    }

    public function pageAction(string $slug)
    {
        $page = $this->pageRepository->getPageBySlug($slug);

        return $this->render('site/page.html.twig', [
            'page' => $page,
        ]);
    }

    public function productAction(Product $product)
    {
        if (!$product) {
            $this->redirectToRoute('app_page', [
                'slug' => 'not-found',
            ]);
        }

        return $this->render('site/product.html.twig', [
            'product' => $product,
        ]);
    }

    public function notFoundAction()
    {
        return $this->render('site/not-found.html.twig');
    }
}
