<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Service\Helper\MediaHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MediaController extends Controller
{
    /** @var MediaHelper */
    private $mediaHelper;

    public function __construct(MediaHelper $mediaHelper)
    {
        $this->mediaHelper = $mediaHelper;
    }

    public function indexAction()
    {
        return $this->render('admin/media/index.html.twig');
    }

    public function getTreeAction(Request $request)
    {
        $id = $request->get('folderId');

        $code = Response::HTTP_OK;
        $response = [];

        try {
            $response = $this->mediaHelper->getFolderTree($id);
        } catch (\Exception $exception) {
            $code = $exception->getCode();
            $response['message'] = $exception->getMessage();
        }

        return new JsonResponse($response, $code);
    }

    public function deleteMediaItemAction(Request $request)
    {
        $id = $request->get('id');
        $this->mediaHelper->deleteMediaItem($id);

        return new JsonResponse(null, Response::HTTP_OK);
    }

    public function deleteFolderAction(Request $request)
    {
        $id = $request->get('id');
        $this->mediaHelper->deleteFolder($id);

        return new JsonResponse(null, Response::HTTP_OK);
    }
}
