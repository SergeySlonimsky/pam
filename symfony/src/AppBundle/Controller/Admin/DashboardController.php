<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->render('admin/dashboard/dashboard.html.twig');
    }

    public function settingsAction()
    {
        return $this->render('admin/dashboard/settings.html.twig');
    }
}
