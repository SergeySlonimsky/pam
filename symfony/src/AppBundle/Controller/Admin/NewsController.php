<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\News;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller
{
    public function indexAction()
    {
        $source = new Entity(News::class);
        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse('admin/news/index.html.twig');
    }
}