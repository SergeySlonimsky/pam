<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Menu;
use AppBundle\Form\MenuCollectionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
    }

    public function generalAction()
    {
        return $this->render('admin/settings/general.html.twig');
    }

    public function menuAction(Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $menus = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository(Menu::class)
            ->findAll();

        $form = $this->createForm(MenuCollectionFormType::class, [
            'menus' => $menus,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            foreach ($form->getData()['menus'] as $menu) {
                $em->persist($menu);
            }
            $em->flush();

            return $this->redirectToRoute('admin_dashboard_index');
        }

        return $this->render('admin/settings/menu.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
