<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Page;
use AppBundle\Form\PageFormType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function indexAction()
    {
        $source = new Entity(Page::class);
        $grid = $this->get('grid');
        $grid->setSource($source);
        $rowActionEdit = new RowAction('Edit', 'admin_page_edit', false);
        $rowActionEdit->setRouteParameters(['id']);
        $grid->addRowAction($rowActionEdit);

        return $grid->getGridResponse('admin/page/index.html.twig');
    }

    public function pageAction(Request $request, Page $page = null)
    {
        if (!$page) {
            $page = new Page();
        }

        $form = $this->createForm(PageFormType::class, $page, [
            'page' => $page,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Page $page */
            $page = $form->getData();
            $this->em->persist($page);
            $this->em->flush();
        }
        return $this->render('admin/page/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function getMenuPagesAction(Request $request)
    {
        $pages = $this->em->getRepository(Page::class)->getByQuery($request->get('q'));
        $result = [];

        array_map(function ($value) use (&$result){
            /** @var $value Page */
            $result[] = [
                'id' => $value->getId(),
                'text' => $value->getTitle(),
            ];
        }, $pages);

        return new JsonResponse($result, Response::HTTP_OK);
    }
}
