<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FolderRepository extends EntityRepository
{
    /**
     * @param string $name
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function finByFolderName(string $name)
    {
        return $this
            ->createQueryBuilder('f')
            ->where('f.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function finRootDir()
    {
        return $this
            ->createQueryBuilder('f')
            ->where('f.parent IS NULL')
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param string $id
     * @return array
     */
    public function removeFolder(string $id)
    {
        return $this->createQueryBuilder('f')
            ->delete('AppBundle:Media\Folder', 'f')
            ->where('f.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}
