<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MediaRepository extends EntityRepository
{
    public function removeMedia(string $id)
    {
        return $this->createQueryBuilder('m')
            ->delete('AppBundle:Media\Media', 'm')
            ->where('m.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}
