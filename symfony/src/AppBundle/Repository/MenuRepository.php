<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MenuRepository extends EntityRepository
{
    public function getByName(string $name)
    {
        return $this
            ->createQueryBuilder('m')
            ->where('m.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getResult();
    }
}
