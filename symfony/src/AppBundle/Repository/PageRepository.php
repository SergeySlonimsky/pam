<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;

class PageRepository extends EntityRepository
{
    /**
     * @param string $menu
     * @param $parent Page| null
     *
     * @return array
     */
    public function getMenuPages(string $menu, $parent = null): array
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.isVisible = TRUE')
            ->join('p.menus', 'm', 'm.page_id = p.id')
            ->where('m.name = :name');

        if ($parent) {
            $qb
                ->andWhere('p.parent = :parent')
                ->setParameter('parent', $parent);
        } else {
            $qb->andWhere('p.parent IS NULL');
        }

        $qb->orderBy('p.priority', 'DESC');

        $qb->setParameter('name', $menu);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $slug
     *
     * @return Page
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPageBySlug(string $slug): Page
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.slug = ?1')
            ->andWhere('p.isVisible = TRUE')
            ->setParameter('1', $slug);

        return $qb->getQuery()->getSingleResult();
    }

    public function getNotFoundPage()
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.slug = "not-found"');

        return $qb->getQuery()->getSingleResult();
    }

    public function getByQuery(string $query)
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->where('p.isVisible = TRUE')
            ->andWhere($qb->expr()->like('p.title', $qb->expr()->literal('%'.$query.'%')))
            ->getQuery()->getResult();
    }
}
