require('../css/global.scss');

window.$ = require('jquery');
window.jQuery = window.$;
window.Popper = require('popper.js');

require('bootstrap');
require('./admin/sb-admin.min');
require('../select2/select2.full.min');
require('../select2/select2entity');

const routes = require('../../web/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);
window.Routing = Routing;


$('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
    if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    let $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');


    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-submenu .show').removeClass("show");
    });


    return false;
});

$(".grid-column-description").each(function () {
    if ($(this).text().length > 50)
        $(this).text($(this).text().substring(0, 50) + '...');
});
$('.grid_body').find('table').addClass('table table-striped table-bordered table-sm');
